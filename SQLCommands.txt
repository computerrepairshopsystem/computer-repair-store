CREATE table UserProfile (

	userID int auto_increment not null,

	permissionFlag int not null, 

	userFirstName varchar(255) not null, 

	userLastName varchar(255) not null, 

	phoneNumber char(10), 

	email varchar(255), 

	dateOfSignup date NOT NULL, 

	primary key (userID)

	) Engine = InnoDB;

CREATE table LoginCredentials (

	userID int not null,

	userName VARCHAR(30) not null,
 
	password VARCHAR(30) not null, 

	primary key(userID), 

	foreign key (userID) references UserProfile(userID) on delete cascade

	) Engine = InnoDB; 

CREATE TABLE Appt (

	appID int not null auto_increment,

	userID int,

	description varchar(150), 

	appTime time not null,

	appDate date not null, 

	primary key(appID), 

	foreign key (userID) references UserProfile(userID) on delete set null

	) Engine = InnoDB;   

CREATE TABLE Flags (
	
	flagID int auto_increment not null, 
	
	flagName varchar(255), 
	
	primary key (flagID)
	
	)Engine = InnoDB;

CREATE TABLE Ticket(
	
	ticketID int not null auto_increment, 
	
	customerID int, 
	
	engineerID int, 
	
	description varchar(255), 
	
	subject varchar(255) not null, 
	
	flagForStatus int not null, 
	
	foreign key (flagForStatus) references Flags(flagID), 
	
	primary key (ticketID), 
	
	foreign key (customerID) references UserProfile(userID) on delete set null,
	
	foreign key (engineerID) references UserProfile(userID) on delete set null
	
	) Engine = InnoDB;

CREATE TABLE userTickets (
	
	ticketID int not null,
	
	userID int, 
	
	foreign key (ticketID) references Ticket(ticketID), 
	
	foreign key (userID) references UserProfile(userID) on delete set null
	
	) Engine = InnoDB;

CREATE TABLE Progress (

	progressID int not null auto_increment, 
	
	primary key (progressID), 
	
	ticketID int not null, 
	
	foreign key (ticketID) references Ticket(ticketID), 
	
	userID int, 
	
	foreign key (userID) references UserProfile(userID) on delete set null, 
	
	timeStamp time not null, 
	
	description varchar(150) not null
	
	) Engine = InnoDB; 