<?php

	echo "<!DOCTYPE html>";
	echo "<html>";
	echo "<head>";
	
  	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
  	echo '<title>Hilltop Computer Repair</title>';
  	echo '<link rel="stylesheet" href="' . asset_url('styles/Style.css') .'" media="screen, projection">';
  	
	echo "</head>";
	echo "<body>";

	//Logo And Dashboard
	echo "<div class = 'header'>";
	echo "<span class='headerContents'>";
	echo "<span class='logo'><image width='425' height='80' alt='logo' src='" . asset_url('images/Company_Logo_For_Top.png') . "'></img></span>";
	
	echo "<span class='topDashboard'>";
	echo "<a class = 'dashboardItem' href='LoginToYourAccount.php'>Login</a>";
	echo "<a class = 'dashboardItem' href='CreateAccount.php'>Create Account</a>";
	echo "</span>";
	
	echo "</span>";
	echo "</div>";

?>