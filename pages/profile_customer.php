<?php

	$this->load->view('templates/dash_logged_in');
	$this->load->view('templates/navbar');
			
	echo "<div class = middleContentHolder>";
		
	echo form_open('UserProfile/index');
	echo "<div class = 'form1'>";
	echo "<div class = 'form1Title'>My Account</div>";
	echo "<div class = 'maLabel1'>Name</div>";
	echo "<span class = 'maInfo1'>$userFirstName $userLastName</span>";
	echo "<div class = 'maLabel1'>Username</div>";
	echo "<span class = 'maInfo1'>$userName</span>";
	echo "<div class = 'maLabel1'>Password</div>";
	echo "<span class = 'maHiddenInfo1'>$password</span>";
	echo "<div class = 'maLabel1'>Email</div>";
	echo "<span class = 'maInfo1'>$email</span>";
	echo "<div class = 'maLabel1'>Phone</div>";
	echo "<span class = 'maInfo1'>$phoneNumber</span>";
	echo "<div class = 'maLabel1'>Date Registered</div>";
	echo "<span class = 'maInfo1'>$dateOfSignup</span>";
	echo "<br>";
	echo "<br>";
	echo "<input type='submit' name ='gotoMakeAppointment' value='Make An Appointment' required>\n";
	echo "<input type='submit' name ='gotoViewTickets' value='View My Tickets' required>\n";
	echo "<input type='submit' name ='gotoChangeAccount' value='Update Account Info' required>\n";
	echo "<input type='submit' name ='gotoChangePassword' value='Change Password' required>\n";
	echo "</div>";
	echo form_close();
			
	echo "</div>";
	
	$this->load->view('templates/footer');

?>