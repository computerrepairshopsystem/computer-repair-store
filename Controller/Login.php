<?php

class Login extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login/Login_model');
		$this->load->helper('url_helper');
	}

	public function index()
	{
		$this->load->helper(array('form', 'url'));
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		//If form data has been submitted then attempt to log in
		if($this->input->post('login')){

			//If form data fits the format, attempt to log in
			if($this->form_validation->run() === TRUE){
				
				//Attempt to log in to their account
				try{
				
					//Log in
					$this->login($this->input->post('username'), $this->input->post('password'));
							
					//Go to user profile controller
					redirect('UserProfile');
					
				}
				//If unable to log in, then display the login error page (They can't log in to that account because the credentials are invalid)
				catch(Exception $e){
				
					$this->load->view('pages/login/login_error');
					
				}
				
			}
			//Otherwise display login error page
			else{
			
				$this->load->view('pages/login/login_error');
				
			}
		
			
		}
		//Otherwise load login page
		else{
		
			$this->load->view('pages/login/login');
		
		}

	}
	
	//Function: login($loginCredentials)
	//Input: Parameter $loginCredentials is a LoginCredentials object, and is not null.
	//Output: None.
	//Description: Logs in to an account, specified by $loginCredentials. Login will be a success if you are not logged in already, and if the credentials (username/password) are valid.
	//Exceptions: Exception 'Error: Invalid login credentials.' will be thrown if credentials are invalid, Exception 'Error: Already logged in to an account.' will be thrown if already logged in.
	public function login($username, $password){
		
		//You can not already be logged in
		if($this->is_logged_in() === false){
		
			//Login credentials must be valid
			if($this->Login_model->isValidCredentials($username, $password)){
			
				$this->setSessionVariable('isLoggedIn', true);
				$this->setSessionVariable('username', $username);
				$this->setSessionVariable('password', $password);
				
			}
			else{
			
				throw new Exception('Error: Invalid login credentials.');
				
			}
			
		}
		else{
		
			throw new Exception('Error: Already logged in to an account.');
		
		}
		
	}
	
	//Function: logout()
	//Input: None.
	//Output: None.
	//Description: Logs out of the account currently logged in. If not currently logged in, logout is a failure, otherwise it is a success.
	//Exceptions: Exception 'Error: Not logged in to an account.' will be thrown if not logged in to an account.
	public function logout(){
	
		if(isLoggedIn()){
		
			//unset($_SESSION['isLoggedIn']);
			//unset($_SESSION['username']);
			//unset($_SESSION['password']);
		
			//calls logout method in MY_Controller unset all session variables
			$this->logOut();
		
		}
		else{
			
			throw new Exception('Error: Not logged in to an account.');
		
		}
		
	}
	
	//Function: getLoginCredentials()
	//Input: No input.
	//Output: A new LoginCredentials object if you are logged in, null if you are not logged in.
	//Description: Returns the login credentials stored on the client-side of whoever is logged in.
	//Exceptions: None.
	public function getLoginCredentials(){
		
		if(isLoggedIn()){
		
			//$lc = new LoginCredentials($_SESSION['username'], $_SESSION['password']);
			$lc = new LoginCredentials($this->getSessionVariable('username'), $this->getSessionVariable('password'));
			return $lc;
			
		}
		else{
		
			return null;
			
		}
		
	}
	
}
?>