<?php

class UserProfile extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('profile/UserProfile_model');
	}

	public function index()
	{
	
		$this->load->helper(array('form', 'url'));
		
		$this->load->library('form_validation');
		
		$data = $this->UserProfile_model->getProfileInfo($_SESSION['username']);
				
		$this->load->view('pages/profile/profile_customer', $data);	

	}
	
	//GET ACCOUNT INFO
	
	//UPDATE ACCOUNT INFO
	
	//CHANGE PASSWORD
	
	
	//createAccount code written by Martin and Colin
	public function createAccount()
	{
		$this->load->helper(array('form', 'url'));
	
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('firstName', 'First Name', 'required');
		$this->form_validation->set_rules('lastName', 'Last Name', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[LoginCredentials.userName]');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]|min_length[8]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phoneAreaCode', 'Phone Number - Area Code', 'required|exact_length[3]|is_natural');
		$this->form_validation->set_rules('phoneFirstThree', 'Phone Number - 3 Digits After Area Code', 'required|exact_length[3]|is_natural');
		$this->form_validation->set_rules('phoneLastFour', 'Phone Number - Last 4 Digits', 'required|exact_length[4]|is_natural');
	
	
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('login');
		}
		else
		{
			//get all information posted by user
			$fname = $this->input->post('firstName');
			$lname = $this->input->post('lastName');
			$username = $this->input->post('username');
			$pw = $this->input->post('password');
			//after submit, upload information to db
			if (isValidCredentials($username, $password)){
				$this->load->controller('UserProfile');
			}
			else{
				//send it back to login
			}
		}
	}
	
	
	//only accessible from admin dashboard
	function viewAllEngineers(){
		$flag = "permissionFlag";
		$permission = $this->getSessionVariable($flag);
		//will show engineer list if user has permission
		if ($permission==2){
			 	
			$data['engineers']= $this->UserProfile_model->getAllEngineers();
			$data['title'] = 'Current Engineeers';
			$this->load->view('engineer/viewEngineers', $data);
		}
			 
		//permission not granted
		else{
			//change this
			$this->load->view('errors/html/error_404');
		}
	}
	
	//only accessible from admin dashboard
	function viewEngineer($userID){
		$flag = "permissionFlag";
		$permission = $this->getSessionVariable($flag);
		//will show engineer list if user has permission
		if ($permission==2){
			$data['eng']= $this->UserProfile_model->getProfileInfoByID($userID);
			$data['title'] = 'Engineer #'.$userID;
			$this->load->view('engineer/viewSingleEngineer', $data);
		}
	
		//permission not granted
		else{
			//change this
			$this->load->view('errors/html/error_404');
		}
	}

	function createEngineer(){
		$flag = "permissionFlag";
		$permission = $this->getSessionVariable($flag);
		//will allow engineer creation if user has permission
		//2 is admin
		if ($permission==2){
			$data['title'] = 'Add Engineer';
		
			$this->form_validation->set_rules('eName', 'Engineer First Name', 'required');
			$this->form_validation->set_rules('eLastName', 'Engineer Last Name', 'required');
			$this->form_validation->set_rules('eUsername', 'Engineer Username', 'required');
			$this->form_validation->set_rules('ePassword', 'Engineer Temporary Password', 'required');
			
			if ($this->form_validation->run() === FALSE){
				
				$this->load->view('engineer/addEngineer');				
			}
			else {
				$eng['fName'] = $this->input->post('eName');
				$eng['lName'] = $this->input->post('eLastName');
				$eng['date'] = date("Y-m-d");
				$eng['username'] = $this->input->post('eUsername');
				$eng['pw'] = $this->input->post('ePassword');
				
				if ($this->UserProfile_model->addEngineer($eng)){

					$data['title'] = 'Engineer Added';
					$data['firstName'] = $eng['fName'];
					$data['lastName'] = $eng['lName'];
					$this->load->view('engineer/successEngineer', $data);
				}
				else{
					Print "error adding engineer";
				}
			}
		}
		//permission not granted
		else{
			$this->load->view('errors/html/error_404');
		}
	}
	
	function deleteEngineer($engId){
		if ($this->UserProfile_model->deleteAppointment($engId)){
			$data['title'] = 'Engineer Successfully Deleted';
			$data['engId'] = $engId;
			$this->load->view('engineer/engineerDeleteSuccess', $data);
		}
		 
		
	}
	
}
?>