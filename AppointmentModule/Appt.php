<?php
class Appt extends MY_Controller {
	


        public function __construct()
        {
                parent::__construct();

                $this->load->model('Appt_model');
                //user_model is where to check credentials? that way inknow to load all tickets or not
                //$this->load->model('user_model');
                $this->load->helper('url_helper');
        }

        
        public function index()
        {
        	$flag = "permissionFlag";
        	$permission = $this->getSessionVariable($flag);
        	//will show appts if user has permission
        	if ($permission==0){
        	
        		$this->form_validation->set_rules('date', 'Date', 'required');
        		
        		$userId = $this->getSessionVariable("userId");
				$data['appts'] = $this->Appt_model->getAppts($userId);
                $data['title'] = 'Appointments';
                
                if ($this->form_validation->run() === FALSE){
                	$this->load->view('templates/header', $data);
                	$this->load->view('appt/navBar', $data);
                	$this->load->view('appt/searchFilter');
                	$this->load->view('appt/index', $data);
                	$this->load->view('templates/footer');
               	}
                 
                else {
                	$date = $this->input->post('date');
                	$data['appts'] = $this->Appt_model->getUserApptsByDate($date, $userId);
                	$data['title'] = 'Appointments | Search Results';
                	 
                	$this->load->view('templates/header', $data);
                	$this->load->view('appt/navBar', $data);
                	$this->load->view('appt/searchFilter');
                	$this->load->view('appt/index', $data);
                	$this->load->view('templates/footer');
                }
        	}
        	//permission not granted
        	else{
        		$this->load->view('errors/html/error_404');
        	}       
        }
        
        public function engineerIndex()
        {
        	//based on user's permissions      	
        	$flag = "permissionFlag";
        	$permission = $this->getSessionVariable($flag);    
        	//permission 1 is engineer
        	if ($permission == 1){   
        		$this->form_validation->set_rules('date', 'Date', 'required');
	          	$data['appts'] = $this->Appt_model->getApptsEngineer();
	        	$data['title'] = 'Appointments';
	        
	        	if ($this->form_validation->run() === FALSE){
	        		$this->load->view('templates/header', $data);
	        		$this->load->view('appt/navBar', $data);
	        		$this->load->view('appt/searchFilter');
	        		$this->load->view('appt/engineerAppt', $data);
	        		$this->load->view('templates/footer');
	        	}
	        	 
	        	else {
	        		$date = $this->input->post('date');
	        		$data['appts'] = $this->Appt_model->getApptsByDate($date);
	        		$data['title'] = 'Appointments | Search Results';
	        
	        		$this->load->view('templates/header', $data);
	        		$this->load->view('appt/navBar', $data);
	        		$this->load->view('appt/searchFilter');
	        		$this->load->view('appt/engineerAppt', $data);
	        		$this->load->view('templates/footer');
	        	}
        	}//permission is not granted
        	else{
        		$this->load->view('errors/html/error_404');
        	}
        }
        
        
		//this happens when a single appointment is clicked to view in more detail
		//originally $slug but I have changed to $id
        public function view($appId)
        {
                $data['single_appt'] = $this->Appt_model->getSingleAppt($appId);
                
                if (empty($data['single_appt']))
                {
                	show_404();
                }
                
                
                $data['heading'] = 'Appointment #'.$data['single_appt']['appID'];
                $data['title'] = 'Appointments';
                $this->load->view('templates/header', $data);
                $this->load->view('appt/navBar', $data);
                $this->load->view('appt/viewAppt', $data);
                $this->load->view('templates/footer');
        }
        
        
        public function createAppt(){
        	$flag = "permissionFlag";
        	$permission = $this->getSessionVariable("permissionFlag");
        	//permission 2 is customer
        	if ($permission ==2){
	        	$this->load->helper('form');
	        	$this->load->library('form_validation');
	        	$data['title'] = 'Choose an Appointment Date';
	        	$date['fName'] = $this->getSessionVariable("firstName"); 
	        	$date['lName'] = $this->getSessionVariable("lastName");
	        	$date['email'] = $this->getSessionVariable("email");
	        	$date['phone'] = $this->getSessionVariable("phone");
	        	
	        	$this->form_validation->set_rules('day', 'Date', 'required');
	        	
	        	if ($this->form_validation->run() === FALSE)
	        	{
	        		$this->load->view('templates/header', $data);
	        		$this->load->view('appt/navBar', $data);
	        		$this->load->view('appt/apptDateForm', $data);
	        		$this->load->view('templates/footer');
	        	}
	        	else
	        	{
	        		$data['date'] = $this->input->post('day');
	        		//set date to $_Session['apptDate']	        		
	        		$date = $data['date'];
	        		$this->setSessionVariable("apptDate", $date);
	        		$data['times']  = $this->Appt_model->getApptsTimesByDate($data['date']);
	        		$this->load->view('templates/header', $data);
	        		$this->load->view('appt/navBar', $data);
	        		$this->load->view('appt/apptTimeGood', $data);
	        		$this->load->view('templates/footer');
	        	}
        	}
        	else{
        		$this->load->view('errors/html/error_404');
        	}
    	
        }
        
                   
        function chooseTime(){
        	$this->load->helper('form');
        	$this->load->library('form_validation');
        	$data['title'] = 'Choose an Appointment Time';
        	$date['fName'] = $this->getSessionVariable("firstName");
        	$date['lName'] = $this->getSessionVariable("lastName");
        	$date['email'] = $this->getSessionVariable("email");
        	$date['phone'] = $this->getSessionVariable("phone");
        	$date['date'] = $this->getSessionVariable("apptDate");
        	unsetSessionVariable("apptDate");
        	
        	$this->form_validation->set_rules('time', 'Time', 'required');
        	$this->form_validation->set_rules('desc', 'Description', 'required');
        	
        	if ($this->form_validation->run() === FALSE)
        	{
        		$this->load->view('templates/header', $data);
        		$this->load->view('appt/apptTimeForm', $data);
        		$this->load->view('templates/footer');
        		 
        	}
        	else
        	{
        		$apptInfo = array(
        				'appID' => 'null',
        				'userID' => $this->getSessionVariable("userId"),
        				'appDate' => $date['date'],
        				'appTime' =>  $this->input->post('time'),
        				'description' => $this->input->post('desc')
        		);
        		       		
        		if ($this->Appt_model->uploadAppt($apptInfo)){
	        		$lastAppt = array(
	        				'appDate' => $data['date'],
	        				'appTime' =>  $this->input->post('time')
	        		);
	        		
	        		$data['apptSuccess']=$this->Appt_model->verifyUpload($lastAppt);
	        		$data['title'] = 'Appointment Successfully Created';
	        		
	        		$this->load->view('templates/header', $data);
	        		$this->load->view('appt/navBar', $data);
	        		$this->load->view('appt/apptSuccess', $data);
	        		$this->load->view('templates/footer');
        		}
        		
        	}
        }
        
        function deleteAppt($appt){
        	$data['single_appt'] = $this->Appt_model->getSingleAppt($appt);
        	$data['date'] = $data['single_appt']['appDate'];
        	$data['time'] = $data['single_appt']['appTime'];
        	
        	$this->Appt_model->deleteAppointment($appt);
        	
        	$data['title'] = 'Appointment Successfully Deleted';
        	
        	$this->load->view('templates/header', $data);
        	$this->load->view('appt/navBar', $data);
        	$this->load->view('appt/apptDeleteSuccess', $data);
        	$this->load->view('templates/footer');
        }
        
      
           	
      
   
}

?>