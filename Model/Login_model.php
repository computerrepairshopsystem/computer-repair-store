<?php
class Login_model extends CI_Model {

        public function __construct()
        {
			//this is the equivalent of typing:
			//require_once "DBConnector.php";
			//this database() is the database file in application/config/database.php
			//where you hardcode your database name, pw, and username
        	$this->load->database();
        }
        
           
        
        //Function: isValidCredentials($username, $password)
        //Output: Returns true or false, based on whether or not the login credentials are valid.
        //Description: Checks if login credentials are valid, as in, they exist in LoginCredentials table in the database.
        //Exceptions: None.
        function isValidCredentials($username, $password){
        
        	//Stores whether login credentials are valid (starts out as false)
        	$isValid = false;
         
        	//Query the LoginCredentials table in the DB for that username with that password
        	$q = $this->$db->query("SELECT * FROM LoginCredentials WHERE userName = '$username' and password = '$password'");
        	$result = $q -> result();
        	//If we got anything back, then we know that the credentials are valid, otherwise they are invalid
        	if($result->num_rows() === 1){
           		$isValid = true;
           	}
           	
        	return $isValid;
        }
        
        //checks whether username already exists in db
        //its purpose is to avoid new users creating a username that already exists
        function isUsernameValid($userName){
        	$isValid = true;
        	$q = $this->db->query("SELECT userID from LoginCredentials where userName = $userName");
        	$result = $q->result();
        	if ($result->num_rows()>0){
        		$isValid = false;
        	}
        	return $isValid;
        }
        
        
        //returns true if successful, otherwise false
        function changePassword($username, $newPassword){
        	$q = "UPDATE LoginCredentials SET password ='".$newPassword.
        			"' WHERE userName ='".$username."'";
        	return $this->db->query($q);
        }
        
        //uploads username and password for a new account
        //returns true if successful, otherwise false
        function uploadNewCredentials($newUsername, $newPassword){
        	$q = $this->db->query("SELECT MAX(userID) FROM UserProfile");
        	$userId = $q->result();
        	return $this->db->query("INSERT INTO LoginCredentials values ($userId, '".
        			$newUsername."', '".$newPassword."')");
        }
        


}
?>