<?php 
if (count($appts)==0){
print "<br>\n 0 appointments found.<br><br>\n";
}
else{
print "<table>\n";
print "<tr> \n <th>ID</th> \n <th>Date</th>\n".
	 "<th>Time</th>\n<th>Description</th>\n".
	"<th>View</th><th>Manage</th></tr>";
foreach ($appts as $appt_item):
print "<tr>\n<td>".$appt_item['appID']."</td>\n".
        "<td>". $appt_item['appDate']."</td>\n".
        "<td>". $appt_item['appTime']."</td>\n".
        "<td>". $appt_item['description']."</td>\n". 
        "<td><a href='".site_url('appt/'.$appt_item['appID']).
		"' role ='button'>View Appointment</a>\n</td>\n".
		"<td><a href='".site_url('deleteAppt/'.$appt_item['appID']).
		"' role ='button'>Delete</a>\n</td>\n".
		"</tr>";
endforeach;
}
print "</table>";
?>