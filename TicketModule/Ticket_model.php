<?php
class Ticket_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }


        public function getTickets($permission, $userId){
           	//customer
        	if($permission == 0){
           		$query = $this->db->get_where('Ticket', array('customerID' => $userId));
        		return $query->result_array();
        	}
        	//if permission is 1 = engineer
        	if($permission == 1){
        		$query = $this->db->get_where('Ticket', array('engineerID' => $userId));
        		return $query->result_array();
        	}
        	//if permission is 2 = admin, they can request all tickets in db
        	else {
        		$query = $this->db->get('Ticket');
        		return $query->result_array();
        	}
        }
        
        function removeTicket($ticketID){
          //removes ticket from the database
 	        return $this->db->delete('Ticket', array('ticketID' =>$ticketID));
          }

        //returns a single ticket
        function getSingleTicket($ticketId){
        	//remember to also return flag's name and progress
        	$query = $this->db->get_where('Ticket', array('ticketID' => $ticketId));
        	return $query->row_array();
        }


        //uploads the just submitted ticket to the database
        //$data is an array of the submitted info
        //this array is created in the Ticket controller
        function uploadATicket($data){
        	return $this->db->insert('Ticket', $data);
        }

        //checks if Ticket was inserted to db
        //by requesting the info just sent
        //$data array is create in the Ticket controller
        function verifyUpload($data){
        	$query = $this->db->get_where('Ticket', $data);
        	return $query->row_array();
        }
        
        function form_insert($data){
           // Inserting in Table(Ticket) of Database
           $this->db->insert('Ticket', $data);
        }
        
        function show_tickets(){
           $query = $this->db->get('Ticket');
           $query_result = $query->result();
           return $query_result;
        }

        // Function To Fetch Selected ticket Record
        function show_ticket_id($data){
         $this->db->select('*');
         $this->db->from('Ticket');
         $this->db->where('ticketID', $data);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
        }

        // Update Query For Selected ticket
        function update_ticket_id1($id,$data){
          $this->db->where('ticketID', $id);
          $this->db->update('Ticket', $data);
        }
        
        function getFlags(){
        	$query = $this->db->get('Flags');
        	return $query->result_array();
        }


        //returns array of progress for said ticket
        function addProgress($user, $desc, $time){
        	$q = $this->db->query("SELECT MAX(ticketID) from Ticket");
        	$ticketId = $q->row_array()['MAX(ticketID)'];        	
        	return $this->db->query("INSERT INTO Progress (ticketID, userID, 
        			timeStamp, description) VALUES (".$ticketId.",". $user.", '".
        			$time."', '".$desc."')");
        }
        
        function addProgressByID($user, $ticketId, $desc, $time){       	 
        	return $this->db->query("INSERT INTO Progress (ticketID, userID, timeStamp, description) VALUES (".$ticketId.",". $user.", '".
        			$time."', '".$desc."')");
        }
        
        //returns array of progress for said ticket
        function getProgress($ticketId){
			$query = $this->db->get_where('Progress', array('ticketID' => $ticketId));
        		return $query->result_array();
        }
        
        function changeStatus($ticketId, $stat){
        	return $this->db->query("UPDATE Ticket SET flagForStatus =".$stat.
        			" WHERE ticketID = '".$ticketId."'");
        }
        
        function changeEngineer($ticketId, $engId){
        	return $this->db->query("UPDATE Ticket SET engineerID =".$engId.
        			" WHERE ticketID = '".$ticketId."'");
        }
        
}

?>