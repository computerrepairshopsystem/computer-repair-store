<?php
class Ticket extends MY_Controller {

        public function __construct()
        {
                parent::__construct();

                $this->load->model('Ticket_model');
		$this->load->model('UserProfile_model');
                $this->load->helper('url_helper');
                
        }
        
        //this is what the ticket module will automatically show when selected
        //or we can change the name of it since the buttons at the dashboard
        // will link to the functions
        public function index()
        {
        		//getTickets will check the user's permissions and return
        		//the tickets based on the permission
        		//e.g. Customers and engineers get their own ticket
        		//e.g. Admin gets all tickets
        		$userId = $this->getSessionVariable("userID");
        		$permission = $this->getSessionVariable("permissionFlag");
		if ($permission === 0){
		$data['flag'] = $this->Ticket_model->getFlags();
                $data['tickets'] = $this->Ticket_model->getTickets($permission, $userId);
                $data['title'] = 'Tickets';

                $this->load->view('tix/index', $data);
		}
	else {
		print "Can't access this page";
	}
        }
        
        public function indexEngi()
        {
           		//getTickets will check the user's permissions and return
        		//the tickets based on the permission
        		//e.g. Customers and engineers get their own ticket
        		//e.g. Admin gets all tickets
        		$userId = $this->getSessionVariable("userID");
        		$permission = $this->getSessionVariable("permissionFlag");
		if ($permission ==1){
		$data['flag'] = $this->Ticket_model->getFlags();
                $data['tickets'] = $this->Ticket_model->getTickets($permission, $userId);
                $data['title'] = 'Tickets';

                $this->load->view('tix/indexEngi', $data);
		}
	else {
		print "Can't access this page";
		}
         }
         
         public function indexAdmin()
         {
         	//getTickets will check the user's permissions and return
         	//the tickets based on the permission
         	//e.g. Customers and engineers get their own ticket
         	//e.g. Admin gets all tickets
         	$userId = $this->getSessionVariable("userID");
         	$permission = $this->getSessionVariable("permissionFlag");
		if ($permission==2){
		$data['flag'] = $this->Ticket_model->getFlags();
         	$data['tickets'] = $this->Ticket_model->getTickets($permission, $userId);
         	$data['title'] = 'Tickets';
         
         	$this->load->view('tix/indexAdmin', $data);
}
	else{
	print "Can't access this page";
         }
}

        public function ticketRemove($ticketId){
          //ticketRemove only accessible by admin
          if($this->getSessionVariable("permissionFlag")==2){
          	$this->Ticket_model->removeTicket($ticketId);
          	$data['title'] = 'Tickets';
          	$data['ticket'] = $ticketId;

          	$this->load->view('tix/ticketRemoveSuccess', $data);
    
          }
          else{
          	print "You can't access this page";
          }

          
        }

		      //this happens when a single ticket is clicked to view in more detail
		        //needs works
		    public function view($ticketId = NULL)
        {
                $data['single_ticket'] = $this->Ticket_model->getSingleTicket($ticketId);

                if (empty($data['single_ticket']))
                {
                	show_404();
                }
                            
                $data['title'] = $data['single_ticket']['ticketID'];
		$data['flag'] = $this->Ticket_model->getFlags();
		$data['permission'] = $this->getSessionVariable("permissionFlag");
              	$data['progress'] = $this->Ticket_model->getProgress($ticketId);
              	
                $this->load->view('tix/viewTicket', $data);                
        }
        
        public function createTicket() {
           //Including validation library
	$userId = $this->getSessionVariable("userID");
         	$permission = $this->getSessionVariable("permissionFlag");
		if ($permission == 1 || $permission==2){

           $this->load->library('form_validation');       
           
           $this->form_validation->set_rules('customerID', 'Customer ID', 'required');
           $this->form_validation->set_rules('desc', 'Description', 'required|min_length[1]|max_length[150]');
           $this->form_validation->set_rules('subject', 'Subject', 'required|min_length[0]|max_length[20]');
           
           if ($this->form_validation->run() == FALSE) {
           		$data['userId'] = $this->getSessionVariable("userID");
           		$this->load->view('tix/create_ticket', $data);
           } 
           else {
             
             $ticketInfo = array(
             	'customerID' => $this->input->post('customerID'),
                'engineerID' => $this->getSessionVariable("userID"),
                'subject' => $this->input->post('subject'),
             	//by defaults sets to new
                'flagForStatus' => 1);
             
             if ($this->Ticket_model->uploadATicket($ticketInfo)){
             	$user = $this->getSessionVariable("userID");
             	$time = date_format(date_create(), 'Y-m-d H:i:s');
             	$desc = $this->input->post('desc');
             	if ($this->Ticket_model->addProgress($user, $desc, $time)){
              		print "<p>Ticket has successfully been created.</p>";
	                 if ($this->getSessionVariable("permissionFlag")==1){
	                	print "<button><a href='".site_url('ticket/indexEngi')."'>View Tickets</a></button>";
	                }
	                if ($this->getSessionVariable("permissionFlag")==2){
	                	print "<button><a href='".site_url('ticket/indexAdmin')."'>View Tickets</a></button>";
	                }
               	}
             	
             }
		else{print "There was an error creating ticket";}
			
             
           }
}
	else{
		print "You can't access this page";}
		
        }

        public function updateTicket($ticketId) {
           	$this->load->library('form_validation');
           	
           	$data['userId'] = $this->getSessionVariable("userID");
           	$this->form_validation->set_rules('flag', 'Status', 'required');
           	$this->form_validation->set_rules('desc', 'Description', 'required');
           	
           	if ($this->form_validation->run() == FALSE) {
           		$data['flag'] = $this->Ticket_model->getFlags();
			$data['ticket'] = $ticketId;
			$data['flag'] = $this->Ticket_model->getFlags();
	       		$this->load->view('tix/update_ticket', $data);
           	}
           	else {
           		$time = date_format(date_create(), 'Y-m-d H:i:s');
           		$desc = $this->input->post('desc');
           		$stat = $this->input->post('flag'); 
           		if($this->Ticket_model->addProgressByID($data['userId'], $ticketId, $desc, $time)){
           			if ($this->Ticket_model->changeStatus($ticketId, $stat)){
           				print "Ticket #". $ticketId. " has successfully been updated.";
           				if ($this->getSessionVariable("permissionFlag")==1){
           					print "<button><a href='".site_url('ticket/indexEngi')."'>View Tickets</a></button>";
           				}
           				if ($this->getSessionVariable("permissionFlag")==2){
           					print "<button><a href='".site_url('ticket/indexAdmin')."'>View Tickets</a></button>";
           				}
           			}
           		}
           		else{
           			print "There was an error updating this ticket.";
           		}
          
           	}           	
        	
          }

        public function modifyTicket($ticketId) {
        	$this->load->library('form_validation');
        	
        	$data['userId'] = $this->getSessionVariable("userID");
        	$this->form_validation->set_rules('newEng', 'New EngineerID', 'required');
               	
        	if ($this->form_validation->run() == FALSE) {
			$data['flag'] = $this->Ticket_model->getFlags();
			 $data['single_ticket'] = $this->Ticket_model->getSingleTicket($ticketId);
        		$this->load->view('tix/modify_ticket', $data);
        	}
        	else {
         	    $newEng = $this->input->post('newEng');
			if ($this->UserProfile_model->getPermissionFlag($newEng)==1){
         	    if ($this->Ticket_model->changeEngineer($ticketId, $newEng)){
         	    	print "Engineer in charge of ticket 
         	    			has successfully been modified.<br>";
			print "<button><a href='".site_url('ticket/indexAdmin')."'>View Tickets</a></button>";
         	    }

         	    else{
         	    	print "There was an error modifying the
         	    			 engineer for this ticket.";
         	    }
}
else { print "This user is not an engineer.";}
        	}
        }
}
?>

        