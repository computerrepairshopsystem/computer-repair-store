<?php
class UserProfile_model extends CI_Model {

        public function __construct(){
        	//this is the equivalent of typing:
			//require_once "DBConnector.php";
			//this database() is the database file in application/config/database.php
			//where you hardcode your database name, pw, and username
        	$this->load->database();
        }
        
        //returns all information from a username such as
        //userID, permissionFlag, userFirstName,     
        //userLastName, phoneNumber, email, and dateOfSignup 
        function getProfileInfo($username){            
        	$q = "SELECT * FROM UserProfile, LoginCredentials WHERE 
        				LoginCredentials.userID = UserProfile.userID AND 
        				LoginCredentials.userName = '" . $username . "'";	
        	$query = $this->db->query($q);	
        	return $query->result_array()[0];
        }
        
        //returns all information from a userID such as
        //userID, permissionFlag, userFirstName,
        //userLastName, phoneNumber, email, and dateOfSignup
        function getProfileInfoByID($userID){
        	$q = "SELECT * FROM UserProfile, LoginCredentials WHERE
        				LoginCredentials.userID = UserProfile.userID AND
        				UserProfile.userID = '" . $userID . "'";
        	$query = $this->db->query($q);
        	return $query->result_array()[0];
        }
        
             
        function getPermissionFlag($userId){   	
			$query = $this->db->query("Select permissionFlag from UserProfile where userID =$userID");
			return $query->result()[0];
        }
        
        //uploads user's info when creating a new account
        //returns true if successful, otherwise false
        function uploadProfileInfo($fName, $lName, $phone, $email, $date){
        	$q = "INSERT into UserProfile VALUES(null, 0,'".$fName."', '".      			
        	$lName."','".$phone."', '".$email."', '".$date."')";
        	return $this->db->query($q);
        }
        
        //changes user's ($userID) old phone number with new phone # ($phone)
        //returns true if successful, otherwise false
        function changePhoneNumber($userID, $phone){
        	return $this->db->query("UPDATE UserProfile SET phoneNumber = '".$phone."' 
        			WHERE userID = $userID");        	
        }
        
        //changes user's ($userID) old phone number with new phone # ($phone)
        //returns true if successful, otherwise false
        function changeEmail($userID, $email){
        	return $this->db->query("UPDATE UserProfile SET email = '".$email."'
        			WHERE userID = $userID");
        }   
        
		//can only be usable by admin 
		//returns an array with all engineers
        public function getAllEngineers(){
        	//permission level
        	$engineer = 1;
        	$query = $this->db->get_where('UserProfile', array('permissionFlag' => $engineer));
        	return $query->result_array();
        }
        
        
        //returns all users that have permission set as engineer
        public function addEngineer($engineerInfo){
        	$engineer = 1;
        	$q = "INSERT into UserProfile(permissionFlag, userFirstName,userLastName, dateOfSignup)
        	values ($engineer, '".$engineerInfo['fName']."', '".$engineerInfo['lName']."', '".$engineerInfo['date'].
        	"')";
        	//if engineer account added successfully
        	if ($this->db->query($q)){
        		$r2 = $this->db->query("SELECT MAX(userID) from UserProfile");
        		$lastUserID =  $r2->result_row()['userID'];
        		 
        		$q2 = "INSERT into LoginCredentials(userID, userName, password)
        		values ($lastUserID,'".$engineerInfo['username']."', '".$engineerInfo['pw']."')";
        
        		return $this->db->query($q2);
        	}
        	//if engineer account could not be added succefully
        	return false;
        }
        
        function deleteUser($userID){
        	return $this->db->delete('UserProfile', array('userID'=>$userID));
        }
 
}
?>