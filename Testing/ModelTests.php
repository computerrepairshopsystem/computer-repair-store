<?php
class ModelTests extends MY_Controller {

        public function __construct()
        {
                parent::__construct();

                $this->load->library('unit_test');
                
        }
        
        function index(){
        	$this->testAppt();
        	$this->testCreateAccountUnique1();
        	$this->testCreateAccountUnique2();
            $this->testChangePassword();
            $this->testAddEngineer();
            $this->testRemoveEngineer();
            $this->removeTestUser();
        }
        
        function testAppt(){
        	$this->load->model('appt/Appt_model');

        	$apptInfo = array(
        			'appID' => 'null',
        			'userID' => "65",
        			'appDate' => "2020/01/01",
        			'appTime' =>  "9:45",
        			'description' => "TESTING CREATE APPT"
        			);

        	$test = $this->Appt_model->uploadAppt($apptInfo);
        	//$this->unit->run($test, $expectedResult, $nameOfTest);
        	echo $this->unit->run($test, true, "Testing Create Appt");
        }
        function testCreateAccountUnique1(){
           $this->load->model('profile/UserProfile_model');
           $fName = 'Ted';
           $lName = 'Cruz';
           $phone = '5126665125';
           $email = 'tCruz@texas.gov';
           $date = '2016/01/01';
          
           $test = $this->UserProfile_model->uploadProfileInfo($fName, $lName, $phone, $email, $date);
           //$this->unit->run($test, $expectedResult, $nameOfTest);
           echo $this->unit->run($test, true, "Testing Create New User part 1");
        }
        function testCreateAccountUnique2(){
          $this->load->model('login/Login_model');

          $userName = 'tCruz';
          $password = 'zodiackiller';
          
          $test = $this->Login_model->uploadNewCredentials($userName, $password);
          //$this->unit->run($test, $expectedResult, $nameOfTest);
          echo $this->unit->run($test, true, "Testing Create New User part 2");
        }
        
        function testChangePassword(){
        $this->load->model('login/Login_model');
        
        $username = 'tCruz';
        $password = 'gunsngod';
        
        $test = $this->Login_model->changePassword($username, $password);
        echo $this->unit->run($test, true, "Testing Change Passowrd.");
        }
        
        function testAddEngineer(){
         $this->load->model('profile/UserProfile_model');
        
         $testInfo = array(
                           'fName' => 'Test',
                           'lName' => 'Engineer',
                           'date' => '2020/01/01',
                           'username' => 'TestEngineer',
                           'pw' => 'testingpw'
                          );
                         
         $test = $this->UserProfile_model->addEngineer($testInfo);
         echo $this->unit->run($test, true, "Testing Creating Engineer");
        }
        
        function testRemoveEngineer(){
         $this->load->model('profile/UserProfile_model');
         $userName = 'TestEngineer';
         $q = "SELECT userID FROM LoginCredentials WHERE userName = '$userName'";
        
         $query = $this->db->query($q);
         $userID = $query->result_array()[0]['userID'];
        
         $test = $this->UserProfile_model->deleteUser($userID);
         echo $this->unit->run($test, true, "Testing Remove Engineer");
        }
        
        function removeTestUser(){
         $this->load->model('profile/UserProfile_model');
         $userName = 'tCruz';
         $q = "SELECT userID FROM LoginCredentials WHERE userName = '$userName'";
        
         $query = $this->db->query($q);
         $userID = $query->result_array()[0]['userID'];
        
         $test = $this->UserProfile_model->deleteUser($userID);
         echo $this->unit->run($test, true, "Removing Test User");
        }
        
        
        function testAddTicket(){
         $this->load->model('tix/Ticket_model');

         $testInfo = array(
                           'customerID' => "1",
                           'engineerID' => "2",
                           'desc' => 'Testing Creating Ticket',
                           'subject' => 'Ticket Upload Test',
                          );

         $test = $this->Ticket_model->uploadATicket($testInfo);
         echo $this->unit->run($test, true, "Testing Creating Ticket");
        }

        function testRemoveTicket(){
         $this->load->model('tix/Ticket_model');
         $ticketID = '1';
         $q = "SELECT ticketID FROM Tickets WHERE ticketID = '$ticketID'";

         $query = $this->db->query($q);
         $userID = $query->result_array()[0]['ticketID'];

         $test = $this->Ticket_model->ticketRemove($ticketID);
         echo $this->unit->run($test, true, "Removing Test Ticket");
        }

        function testUpdateTicket(){
         $this->load->model('tix/Ticket_model');

         $testInfo = array(
                           'flag' => "2",
                           'desc' => 'Updating Ticket',
                          );

         $test = $this->Ticket_model->update_ticket_id1($testInfo);
         echo $this->unit->run($test, true, "Testing Updating Ticket");
        }
        
}
?>