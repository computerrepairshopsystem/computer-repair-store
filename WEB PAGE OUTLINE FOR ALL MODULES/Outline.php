<?php
	
	
	require_once "TopDashboard.php";
	
	echo "<!DOCTYPE html>";
	echo "<html>";
	echo "<head>";
	
  	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
  	echo '<title>RAMS Computer Repair</title>';
  	echo '<link rel="stylesheet" href="Style.css" media="screen, projection">';
  	
	echo "</head>";
	echo "<body>";
	
	//Logo And Dashboard
	echo "<div class = 'topBar1'>";
	echo "<span class='topBarContents'>";
	echo "<span class='logo'><image width='425' height='80' alt='logo' src='Company_Logo_For_Top.png'></img></span>";
	printTopDashboard(); //Different depending on class of user viewing the page (or if user is logged out, aka a guest)
	echo "</div>";
	echo "</div>";
	
	//Site Navigation Bar
	echo "<div class = 'topBar2'>";
	echo "<a class = 'topBar2Link' href='nowhere'>Home</a>";
	echo "<a class = 'topBar2Link' href='nowhere'>Help Me Get Started</a>";
	echo "<a class = 'topBar2Link' href='nowhere'>What We Do</a>";
	echo "<a class = 'topBar2Link' href='nowhere'>Contact Us</a>";
	echo "<a class = 'topBar2Link' href='nowhere'>News Bulletin</a>";
	echo "</div>";

	//Middle Content Area
	echo "<div class = 'middleContentHolder'>";
	printSampleContent();
	echo "</div>";
	
	//Bottom (For Contact Info, Copyright, Etc.)
	echo "<div class = 'bottomContentHolder'>";
	echo "<div class='brag'>Over 1,000,000 Computers Repaired (This month alone!)</div>";
	echo "<div class='credits'>Created in 2016 by Martin Celusniak, Colin Chandler, Janie Coronado, and Sam Duncan. Site creation supervised by Dr. Ali.</div>";
	echo "<div class='copyright'>&copy 2016</div>";
	echo "</div>";
		
	echo "</body>";
	echo "</html>";
	
	function printSampleContent(){
	
		echo "<div class = 'content1'>";
		echo "<span><image width='425' height='80' alt='logo' src='Company_Logo_For_Top.png'></img></span>";
		echo "<br><span>WELCOME CITIZEN! Hilltop Computer Repair is your place for computer repair. <br> I will finish this later, and add padding, that's not working for now but I will find out why.</span>";
		echo "</div>";
	
	}
		
?>