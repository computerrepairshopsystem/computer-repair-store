<?php
class Appt_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
        
        
        //have changed $slug to $id, see what this does
        //where will I grab the userId from?
        //this is to obtain the correct apptms
        public function getAppts($userId){       
        	$query = $this->db->get_where('Appt', array('userID' => $userId));
        	return $query->result_array();
        }
        
        public function getApptsEngineer(){
        	$query = $this->db->get('Appt');
        	return $query->result_array();
        }
                
        //returns appointment based on appId sent in
        function getSingleAppt($appId){
        	$query = $this->db->get_where('Appt', array('appID' => $appId));
        	return $query->row_array();
        }
        
        //returns all appointment in an array that
        //will be occuring on the requested date in parameter
        //this is used by the user only
        function getUserApptsByDate($date, $userId){
        	$query = $this->db->get_where('Appt', array('appDate' => $date, 'userID'=>$userId
        	));
        	return $query->result_array();
        }
        
        //returns all appointment in an array that
        //will be occuring on the requested date in parameter
        //this is used by the engineer only
        function getApptsByDate($date){
        	$query = $this->db->get_where('Appt', array('appDate' => $date));
        	return $query->result_array();
        }
        
        //returns all appointment times in an array that
        //will be occuring on the requested date in parameter
        function getApptsTimesByDate($date){
        	$q = "Select appTime from Appt where appDate = '".$date."'";
        	$query = $this->db->query($q);
        	return $query->result_array();
        }
        
        //uploads the just submitted appt to database
        //return true if successful
        function uploadAppt($data){
        	return $this->db->insert('Appt', $data);
        }
        
        //checks if appt was inserted to db
        function verifyUpload($data){
        	$query = $this->db->get_where('Appt', $data);
        	return $query->row_array();
        }
        
        function deleteAppointment($apptId){
			$this->db->delete('Appt', array('appID'=>$apptId));
         }
}

?>