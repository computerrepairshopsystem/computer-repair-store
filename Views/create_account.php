<?php

	$this->load->view('templates/header');
	$this->load->view('templates/navbar');

	echo "<div class = 'middleContentHolder'>";
	
	echo form_open('CreateAccount/index');
	echo "<div class = 'form1'>";
	echo "<div class = 'form1Title'>Create Account</div>";
	echo "<div class = 'caEntryLabel1'>Name</div>";
	echo "<input class = 'caEntry' type='text' name ='firstName' placeholder='First' value='' required>\n";
	echo "<input class = 'caEntry' type='text' name ='lastName' placeholder='Last' value='' required>\n";
	echo "<div class = 'caEntryLabel1'>Username</div>";
	echo "<input class = 'caEntry' type='text' name ='username' placeholder='' value='' required>\n";
	echo "<div class = 'caEntryLabel1'>Password</div>";
	echo "<input class = 'caEntry' type='password' name ='password' placeholder='' value='' required>\n";
	echo "<div class = 'caEntryLabel1'>Email</div>";
	echo "<input class = 'caEntry' type='text' name ='email' placeholder='guy@example.com' value='' required>\n";
	echo "<div class = 'caEntryLabel1'>Phone</div>";
	echo "<input class = 'caPhoneAreaCode' type='text' name ='phoneAreaCode' placeholder='512' value='' required>\n";
	echo " - ";
	echo "<input class = 'caPhoneFirst3' type='text' name ='phoneFirst3' placeholder='000' value='' required>\n";
	echo " - ";
	echo "<input class = 'caPhoneLast4' type='text' name ='phoneLast4' placeholder='0000' value='' required>\n";
	echo "<br>";
	echo "<input type='submit' name ='createAccount' value='Create Account' required>\n";
	echo "</div>";
	echo form_close();
		
	echo "</div>";
	
	$this->load->view('templates/footer');
	
?>