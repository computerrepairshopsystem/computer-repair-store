<?php class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //this is the equivalent of $session_start()
        $this->load->library('session');
    }

    //checks if a username has been set
    //returns true if username is set, meaning they are logged in successfully
  	//returns false if 'userName' index in $_session array is empty/null
    public function is_logged_in()
    {
        return isset($_SESSION['username']);
    }
    
    //will unset all values in $_session array
    public function logOut()
    {
    	unset($_SESSION['userId']);
    	unset($_SESSION['userName']);
    	unset($_SESSION['firstName']);
    	unset($_SESSION['lastName']);
    	unset($_SESSION['phone']);
    	unset($_SESSION['email']);
    	unset($_SESSION['permissionFlag']);
    }
    
    
    
    //adds a new key and value to the $_session array
    //example: setSessionVariable('userName', $userName);
    ///r.v. $_SESSION['userName') = $userName
    public function setSessionVariable($key, $value){
    	return $this->session->set_userdata("$key", "$value");
    }
    
    //returns value set in a key inside $_session array
    //example:
    // $_session = array(
    //  'userName'  => 'johndoe',
    //  'email'     => 'johndoe@some-site.com');
    // getSessionVariable('userName');
    ///r.v. = johndoe
    public function getSessionVariable($key){
    	return $this->session->userdata("$key");
    }
    
    //unsets any variable that has been set
    //for example: a temporary variable that just holds quick info
    //e.g. holding a date
    public function unsetSessionVariable($key){
    	unset($_SESSION["$key"]);
    }
}

?>