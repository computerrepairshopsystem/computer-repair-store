<?php

	$this->load->view('templates/dash_logged_in');
	$this->load->view('templates/navbar');

	echo "<div class = 'middleContentHolder'>";
	
	echo form_open('UserProfile/createAccount');
	echo "<div class = 'form1'>";
	echo "<div class = 'form1Title'>Create Account</div>";
	echo "<div class = 'caEntryLabel1'>Name</div>";
	echo "<input class = 'caEntry' type='text' name ='firstName' placeholder='First' value='" . set_value('firstName') . "' required>\n" . form_error('firstName');
	echo "<input class = 'caEntry' type='text' name ='lastName' placeholder='Last' value='" . set_value('lastName') . "' required>\n" . form_error('lastName');
	echo "<div class = 'caEntryLabel1'>Email</div>";
	echo "<input class = 'caEntry' type='text' name ='email' placeholder='guy@example.com' value='" . set_value('email') . "' required>\n" . form_error('email');
	echo "<div class = 'caEntryLabel1'>Phone</div>";
	echo "<input class = 'caPhoneAreaCode' type='text' name ='phoneAreaCode' placeholder='512' maxlength='3' value='" . set_value('phoneAreaCode') . "' required>\n" . form_error('phoneAreaCode');
	echo " - ";
	echo "<input class = 'caPhoneFirst3' type='text' name ='phoneFirstThree' placeholder='000' maxlength='3' value='" . set_value('phoneFirstThree') . "' required>\n" . form_error('phoneFirstThree');
	echo " - ";
	echo "<input class = 'caPhoneLast4' type='text' name ='phoneLastFour' placeholder='0000' maxlength='4' value='" . set_value('phoneLastFour') . "' required>\n" . form_error('phoneLastFour');
	echo "<br>";
	echo "<input type='submit' name ='createAccount' value='Create Account' required>\n";
	echo "</div>";
	echo form_close();
		
	echo "</div>";
	
	$this->load->view('templates/footer');
	
?>